# post_man/serializers.py
from rest_framework import serializers
from . import models


class CustomerSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('id', 'name', 'dob', 'created_at', 'updated_at',)
        model = models.Customer