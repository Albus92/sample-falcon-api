# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import generics
from rest_framework.views import APIView
from .models import Customer
from .serializers import CustomerSerializer
from rest_framework import authentication, permissions


# Create your views here.
class CreateCustomerAPIView(APIView):
	authentication_classes = (authentication.TokenAuthentication,)

	def post(self, request):
		user = request.data
		serializer = CustomerSerializer(data=user)
		serializer.is_valid(raise_exception=True)
		serializer.save()
		return Response(serializer.data, status=status.HTTP_201_CREATED)

class CreateRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
 
    # Allow only authenticated users to access this url
    authentication_classes = (authentication.TokenAuthentication,)
    serializer_class = CustomerSerializer
 
    def get(self, request, *args, **kwargs):
        # serializer to handle turning our `User` object into something that
        # can be JSONified and sent to the client.
        serializer = self.serializer_class(request.user)
 
        return Response(serializer.data, status=status.HTTP_200_OK)
 
    def put(self, request, *args, **kwargs):
        serializer_data = request.data.get('user', {})
 
        serializer = CustomerSerializer(
            request.user, data=serializer_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
 
        return Response(serializer.data, status=status.HTTP_200_OK)

class CustomerList(generics.ListAPIView):
    queryset = Customer.objects.all().order_by('-dob')
    serializer_class = CustomerSerializer


class CustomerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

