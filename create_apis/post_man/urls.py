from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^', views.CustomerList.as_view()),
    url(r'^<int:pk>/$', views.CustomerDetail.as_view()),
    url(r'^create/$', views.CreateCustomerAPIView.as_view()),
    url(r'^update/$', views.CreateRetrieveUpdateAPIView.as_view()),
]