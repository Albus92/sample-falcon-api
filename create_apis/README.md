This is guide for how to install my app and how to using it
*** Installing virtualenv
virtualenv is used to manage Python packages for different projects. Using virtualenv allows you to avoid installing Python packages globally which could break system tools or other projects. You can install virtualenv using pip.

On macOS and Linux:

python3 -m pip install --user virtualenv
On Windows:

py -m pip install --user virtualenv
Note If you are using Python 3.3 or newer the venv module is included in the Python standard library. This can also create and manage virtual environments, however, it only supports Python 3.
Creating a virtualenv
virtualenv allows you to manage separate package installations for different projects. It essentially allows you to create a “virtual” isolated Python installation and install packages into that virtual installation. When you switch projects, you can simply create a new virtual environment and not have to worry about breaking the packages installed in the other environments. It is always recommended to use a virtualenv while developing Python applications.

To create a virtual environment, go to your project’s directory and run virtualenv.

On macOS and Linux:

python3 -m virtualenv env
On Windows:

py -m virtualenv env
The second argument is the location to create the virtualenv. Generally, you can just create this in your project and call it env.

virtualenv will create a virtual Python installation in the env folder.

Note You should exclude your virtualenv directory from your version control system using .gitignore or similar.
Activating a virtualenv
Before you can start installing or using packages in your virtualenv you’ll need to activate it. Activating a virtualenv will put the virtualenv-specific python and pip executables into your shell’s PATH.

On macOS and Linux:

source env/bin/activate
On Windows:

.\env\Scripts\activate
You can confirm you’re in the virtualenv by checking the location of your Python interpreter, it should point to the env directory.

On macOS and Linux:

which python
.../env/bin/python
On Windows:

where python
.../env/bin/python.exe
As long as your virtualenv is activated pip will install packages into that specific environment and you’ll be able to import and use packages in your Python application.

Leaving the virtualenv
If you want to switch projects or otherwise leave your virtualenv, simply run:

deactivate

*** run requirements.txt file inside env:

pip install -r requirements.txt

*** create postgresql database


Setting Up the Database
We are going to use the PostgreSQL database because it's more stable and robust.

Create the test_albus database and assign a user.

Switch over to the Postgres account on your machine by typing:

sudo su postgres
Access the Postgres prompt and create the database:

psql
postgres=# CREATE DATABASE test_albus;
Create a role:

postgres=# CREATE ROLE albus WITH LOGIN PASSWORD 'albus';
Grant database access to the user:

postgres=# GRANT ALL PRIVILEGES ON DATABASE auth TO albus;

****inside folder create_apis run
run create migrations file:

python manage.py makemigrations
apply migrate file

python manage.py migrate

Creating a Superuser
Create a superuser by running the following command:

python manage.py createsuperuser


*** start app, inside folder create_apis run:

python manage.py runserver

*** view admin page
http://127.0.0.1:8000/admin/
login with superuser and pass you have created

*** view list:
http://127.0.0.1:8000/api/
*** view detail
http://127.0.0.1:8000/api/{id}
*** Create:
http://127.0.0.1:8000/api/create/
*** update:
http://127.0.0.1:8000/api/update/

